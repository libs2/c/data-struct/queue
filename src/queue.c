//author Luke Paulson
//date July 7, 2020
//copyright The MIT License (MIT)
//version 

#include "queue.h"

/****************** New Queue ************************************************/

Queue_Char_s* new_Char_queue(void){
  Queue_Char_s* self = NULL;

  //Ensure memory allocation of new queue
  self = (Queue_Char_s*)malloc(sizeof(Queue_Char_s));
  if(self == NULL){
    return NULL;
  }
  //Init empty queue
  self->front = NULL;
  self->back = NULL;

  //Size of queue - Not to be used for is_empty function
  self->size = 0;

  //Attach funtion pointers
  self->enqueue = &enqueue_Char;
  self->dequeue = &dequeue_Char;
  self->peek = &peek_Char;
  self->isEmpty = &is_empty_Char;
  self->delete = &delete_Char;

  return self;
}

Queue_U8_s* new_U8_queue(void){
  Queue_U8_s* self = NULL;

  //Ensure memory allocation of new queue
  self = (Queue_U8_s*)malloc(sizeof(Queue_U8_s));
  if(self == NULL){
    return NULL;
  }
  //Init empty queue
  self->front = NULL;
  self->back = NULL;

  //Size of queue - Not to be used for is_empty function
  self->size = 0;

  //Attach funtion pointers
  self->enqueue = &enqueue_U8;
  self->dequeue = &dequeue_U8;
  self->peek = &peek_U8;
  self->isEmpty = &is_empty_U8;
  self->delete = &delete_U8;

  return self;
}

/****************** New Node *************************************************/

static struct Node_Char_s* new_Char_node(char val){
  struct Node_Char_s* self = NULL;

  //Ensure memory allocation
  self = (struct Node_Char_s*)malloc(sizeof(struct Node_Char_s));
  if(self == NULL){
    return NULL;
  }

  //Next points to nothing
  self->next = NULL;

  //Assign value
  self->val = val;

  return self;
}

static struct Node_U8_s* new_U8_node(uint8_t val){
  struct Node_U8_s* self = NULL;

  //Ensure memory allocation
  self = (struct Node_U8_s*)malloc(sizeof(struct Node_U8_s));
  if(self == NULL){
    return NULL;
  }

  //Next points to nothing
  self->next = NULL;

  //Assign value
  self->val = val;

  return self;
}

/****************** Enqueue **************************************************/

static void enqueue_Char(Queue_Char_s* self, char val){
  //If the queue is empty the front and back point to the same node
  if(is_empty_Char(self)){
    self->front = new_Char_node(val);
    self->back = self->front;
  }
  //Last node of queue points to new node then moves back pointer
  else{
    self->back->next = new_Char_node(val);
    self->back = self->back->next;
  }
  
  //Increment size - if size is less than max number - otherwise its big
  if(self->size < (size_t)-1){
    self->size++;
  }
  return;
}

static void enqueue_U8(Queue_U8_s* self, uint8_t val){
  //If the queue is empty the front and back point to the same node
  if(is_empty_U8(self)){
    self->front = new_U8_node(val);
    self->back = self->front;
  }
  //Last node of queue points to new node then moves back pointer
  else{
    self->back->next = new_U8_node(val);
    self->back = self->back->next;
  }
  
  //Increment size - if size is less than max number - otherwise its big
  if(self->size < (size_t)-1){
    self->size++;
  }
  return;
}

/****************** Dequeue **************************************************/

static char dequeue_Char(Queue_Char_s* self){  
  //If the queue is empty return 0
  if(is_empty_Char(self)){
    return 0;
  }
  
  //Temp node
  struct Node_Char_s* temp_node = NULL;
  //Return value
  char ret_val = self->front->val;

  //Get pointer to next node in queue
  temp_node = self->front->next;
  //Free front node
  free(self->front);
  //point front of queue to temp_node ie. next node
  self->front = temp_node;

  //If Queue is empty point back to null
  if(is_empty_Char(self)){
    self->back = NULL;
  }

  //reduce queue size
  if(self->size > 0){
    self->size--;
  }

  return ret_val;
}

static uint8_t dequeue_U8(Queue_U8_s* self){  
  //If the queue is empty return 0
  if(is_empty_U8(self)){
    return 0;
  }
  
  //Temp node
  struct Node_U8_s* temp_node = NULL;
  //Return value
  uint8_t ret_val = self->front->val;

  //Get pointer to next node in queue
  temp_node = self->front->next;
  //Free front node
  free(self->front);
  //point front of queue to temp_node ie. next node
  self->front = temp_node;

  //If Queue is empty point back to null
  if(is_empty_U8(self)){
    self->back = NULL;
  }

  //reduce queue size
  if(self->size > 0){
    self->size--;
  }

  return ret_val;
}


/****************** Peek *****************************************************/

static char peek_Char(Queue_Char_s* self){
  //If the queue is empty return 0
  if(is_empty_Char(self)){
    return 0;
  }
  
  return self->front->val;
}

static uint8_t peek_U8(Queue_U8_s* self){
  //If the queue is empty return 0
  if(is_empty_U8(self)){
    return 0;
  }
  
  return self->front->val;
}

/****************** Is Empty *************************************************/

static bool is_empty_Char(Queue_Char_s* self){
  //If front points to NULL the queue is empty
  if(self->front == NULL){
    return true;
  }
  return false;
}

static bool is_empty_U8(Queue_U8_s* self){
  //If front points to NULL the queue is empty
  if(self->front == NULL){
    return true;
  }
  return false;
}

/****************** Delete ***************************************************/

static void delete_Char(Queue_Char_s* self){
  //While the queue is not empty free all nodes
  while(!is_empty_Char(self)){
    dequeue(self);
  }
  free(self);
  return;
}

static void delete_U8(Queue_U8_s* self){
  //While the queue is not empty free all nodes
  while(!is_empty_U8(self)){
    dequeue(self);
  }
  free(self);
  return;
}