//! \file
//! \addtogroup QUEUE
//! \brief Source code for queue data structure
//! \details 
//! \author Luke Paulson
//! \date July 7, 2020
//! \copyright The MIT License (MIT) 
//! \version


#ifndef QUEUE_H
#define QUEUE_H

#include <stdint.h>
#include <stdlib.h>
#include <malloc.h>
#include <stdbool.h>

/*! \name Macro Functions
    \param q Queue pointer
    \param v Value
*/
//! \{
#define enqueue(q,v)    q->enqueue(q,v)                //!< \ref eq "See Enqueue"
#define dequeue(q)      q->dequeue(q)                  //!< \ref dq "See Dequeue"
#define peek(q)         q->peek(q)                     //!< \ref pq "See Peek"
#define is_empty(q)     q->isEmpty(q)                  //!< \ref ie "See Is Empty"
#define delete_queue(q) q->delete(q)                   //!< \ref delq "See Delete"
//! \}

typedef struct Queue_Char_s Queue_Char_s;
typedef struct Queue_U8_s Queue_U8_s;

//! \name Enqueue function pointer
//! \{
typedef void (*fptrEnqueue_Char)(Queue_Char_s*, char);
typedef void (*fptrEnqueue_U8)(Queue_U8_s*, uint8_t);
//! \}

//! \name Dequeue function pointer
//! \{
typedef char (*fptrDequeue_Char)(Queue_Char_s*);
typedef uint8_t (*fptrDequeue_U8)(Queue_U8_s*);
//!\ \}

//! \name Peek Function pointer
//! \{
typedef char (*fptrPeek_Char)(Queue_Char_s*);
typedef uint8_t (*fptrPeek_U8)(Queue_U8_s*);
//! \}

//! \name Is_empty function pointer
//! \{
typedef bool (*fptrIsEmpty_Char)(Queue_Char_s*);
typedef bool (*fptrIsEmpty_U8)(Queue_U8_s*);
//! \}

//! \name Delete function pointer
//! \{
typedef void (*fptrDelete_Char)(Queue_Char_s*);
typedef void (*fptrDelete_U8)(Queue_U8_s*);
//! \}

//! \name Queue base structure
//! \{

//! Character queue structure
struct Queue_Char_s{
  struct Node_Char_s* front;                           //!< Front of queue
  struct Node_Char_s* back;                            //!< Back of queue

  size_t size;                                          //!< size of queue

  fptrEnqueue_Char enqueue;                            //!< Pointer to enqueue function
  fptrDequeue_Char dequeue;                            //!< Pointer to dequeue function
  fptrPeek_Char peek;                                  //!< Pointer to peek function
  fptrIsEmpty_Char isEmpty;                            //!< Pointer to is_empty function
  fptrDelete_Char delete;                              //!< Pointer to delete function
};

//! Unsigned 8bit integer queue structure
struct Queue_U8_s{
  struct Node_U8_s* front;                           //!< Front of queue
  struct Node_U8_s* back;                            //!< Back of queue

  size_t size;                                       //!< size of queue

  fptrEnqueue_U8 enqueue;                            //!< Pointer to enqueue function
  fptrDequeue_U8 dequeue;                            //!< Pointer to dequeue function
  fptrPeek_U8 peek;                                  //!< Pointer to peek function
  fptrIsEmpty_U8 isEmpty;                            //!< Pointer to is_empty function
  fptrDelete_U8 delete;                              //!< Pointer to delete function
};
//! \}


//! \name Data node structure
//! \{

//! Character node
static struct Node_Char_s{
  struct Node_Char_s* next;                            //!< Pointer next node structure
  char val;                                            //!< Value of node
}Node_Char_s;

//! Unsigned 8bit node
static struct Node_U8_s{
  struct Node_U8_s* next;                               //!< Pointer next node structure
  uint8_t val;                                          //!< Value of node
}Node_U8_s;
//! \}


/*!   \anchor nq
      \name New Queue Base Structure
      \brief Create note empty queue sturcture

      \return Pointer to Queue object
*/
//!{
Queue_Char_s* new_Char_queue(void);               //!< Create new Char queue
Queue_U8_s* new_U8_queue(void);                   //!< Create new uint8_t queue
//!   \}

/*!   \anchor nn
      \name New Queue Node
      \brief Create a new type specific data nodes, this is done automatically 
      when enqueue is called

      \param val value to add
      \return pointer to new data node
*/
//! \{
static struct Node_Char_s* new_Char_node(char val);
static struct Node_U8_s* new_U8_node(uint8_t val); 
//! \}

/*!   \anchor eq
      \name Enqueue Value
      \brief Add new value to the back of a queue

      \param self pointer to queue being used
      \param val value to push to queue
*/
//! \{
static void enqueue_Char(Queue_Char_s* self, char val);
static void enqueue_U8(Queue_U8_s* self, uint8_t val);
//! \}

/*!   \anchor dq
      \name Dequeue Value
      \brief Remove value from front of queue
      \note if queue is empty dequeue will return 0

      \param self pointer to queue being used
      \return val value returned from queue
*/
//! \{
static char dequeue_Char(Queue_Char_s*);
static uint8_t dequeue_U8(Queue_U8_s*);
//! \}

/*!   \anchor pq
      \name Peek at Queue
      \brief Look at front of queue without removing value
      \note if queue is empty dequeue will return 0

      \param self pointer to queue being used
      \return val value at front of queue
*/
// \{
static char peek_Char(Queue_Char_s*);
static uint8_t peek_U8(Queue_U8_s*);
//! \}

/*!   \anchor ie
      \name Is Queue Empty
      \brief Check if the queue is empty

      \param self pointer to queue being checked
      \retval true queue is empty
*/
//! \{
static bool is_empty_Char(Queue_Char_s*);
static bool is_empty_U8(Queue_U8_s*);
//! \}

/*!   \anchor delq
      \name Delete Queue
      \brief Delete the queue

      \param self pointer to queue being deleted

*/
//! \{
static void delete_Char(Queue_Char_s*);
static void delete_U8(Queue_U8_s*);
//! \}

#endif // QUEUE_H