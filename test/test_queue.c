//! \file
//! \addtogroup TEST
//! \brief Unit test functions build for Ceedling
//! \author Luke Paulson
//! \date 
//! \copyright The MIT License (MIT)
//! \version

#include "unity.h"

#include "queue.h"

Queue_Char_s* test_char_queue = NULL;
Queue_U8_s* test_u8_queue = NULL;

void setUp(void)
{
    test_char_queue = new_Char_queue();
    test_u8_queue = new_U8_queue();
}

void tearDown(void)
{
    delete_queue(test_char_queue);
    delete_queue(test_u8_queue);
}

//! \test New queue front points to NULL
void test_new_queue_points_to_null(void){
    TEST_ASSERT_NULL(test_char_queue->front);
    TEST_ASSERT_NULL(test_u8_queue->front);
}

//! \test New queue size is 0
void test_new_queue_size_is_zero(void){
    TEST_ASSERT_EQUAL_UINT8(0, test_char_queue->size);
    TEST_ASSERT_EQUAL_UINT8(0, test_u8_queue->size);
}

//! \test New queue is empty
void test_new_queue_is_empty(void){
    TEST_ASSERT_TRUE(is_empty(test_char_queue));
    TEST_ASSERT_TRUE(is_empty(test_u8_queue));
}

//! \test Queue is not empty after enqueue
void test_queue_is_not_empty_after_enqueue(void){
    enqueue(test_char_queue, 'c');
    enqueue(test_u8_queue, 42);
    TEST_ASSERT_FALSE(is_empty(test_char_queue));
    TEST_ASSERT_FALSE(is_empty(test_u8_queue));

}

//! \test New queue returns 0
void test_new_queue_returns_zero(void){
    TEST_ASSERT_EQUAL_CHAR(0, dequeue(test_char_queue));
    TEST_ASSERT_EQUAL_UINT8(0, dequeue(test_u8_queue));
}

//! \test Enqueue increments size by 1
void test_enqueue_increase_queue_by_one(void){
    enqueue(test_char_queue, 'c');
    TEST_ASSERT_EQUAL_UINT8(1, test_char_queue->size);

    enqueue(test_u8_queue, 42);
    TEST_ASSERT_EQUAL_UINT8(1, test_u8_queue->size);
}
//! \test Dequeu decreses size by 1
void test_dequeue_decrese_size_by_one(void){
    enqueue(test_char_queue, 'c');
    dequeue(test_char_queue);
    TEST_ASSERT_EQUAL_UINT8(0, test_char_queue->size);

    enqueue(test_u8_queue, 42);
    dequeue(test_u8_queue);
    TEST_ASSERT_EQUAL_UINT8(0, test_u8_queue->size);
}
   
//! \test Dequeue returns Enque value (single)
void test_dequeue_returns_the_enqueue_value(void){
    enqueue(test_char_queue, 'c');
    TEST_ASSERT_EQUAL_CHAR('c', dequeue(test_char_queue));

    enqueue(test_u8_queue, 42);
    TEST_ASSERT_EQUAL_UINT8(42, dequeue(test_u8_queue));
}

//! \test Peek gets the front queue value
void test_peek_get_the_queue_front_value(void){
    enqueue(test_char_queue, 'c');
    TEST_ASSERT_EQUAL_CHAR('c', peek(test_char_queue));

    enqueue(test_u8_queue, 42);
    TEST_ASSERT_EQUAL_CHAR(42, peek(test_u8_queue));
}

//! \test Peek doesn't change queue size
void test_peek_dosent_change_size(void){
    enqueue(test_char_queue, 'c');
    peek(test_char_queue);
    TEST_ASSERT_EQUAL_UINT8(1, test_char_queue->size);

    enqueue(test_u8_queue, 42);
    peek(test_u8_queue);
    TEST_ASSERT_EQUAL_UINT8(1, test_u8_queue->size);
}

//! \test Enqueue 100 valuse increase size by 100
void test_enqueue_one_multiple_size(void){
    for(uint8_t i = 0; i < 25; i++){
        enqueue(test_char_queue, 'c');
        enqueue(test_u8_queue, i);
    }
    TEST_ASSERT_EQUAL_UINT8(25, test_char_queue->size);
    TEST_ASSERT_EQUAL_UINT8(25, test_u8_queue->size);
}
//! \test Enqueue 100 values then dequeue 100 values
void test_enqueue_dequeue_multiple(void){
    for(uint8_t i = 0; i < 25; i++){
        enqueue(test_char_queue, i);
        enqueue(test_u8_queue, i);
    }

    for(uint8_t i = 0; i < 25; i++){
        TEST_ASSERT_EQUAL_UINT8(i, dequeue(test_char_queue));
        TEST_ASSERT_EQUAL_UINT8(i, dequeue(test_u8_queue));
    }

}
